﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите ширину прямоугольника (м) : ");
            string v1 = Console.ReadLine();
            double a = Convert.ToDouble(v1);

            Console.WriteLine("Введите длину прямоугольника (м) : ");
            string v2 = Console.ReadLine();
            double b = Convert.ToDouble(v2);

            double p = a * b;
            Console.WriteLine("Площадь прямоугольника (м2) = ");
            Console.WriteLine(p);
        }
    }
}
