﻿using System;

    class Program
{
        static void Main(string[] args)
    {
        Console.WriteLine("Введите число: ");
        string s = Console.ReadLine();
        int i = Convert.ToInt32(s);

        if (i % 2 == 0)
        {
            Console.WriteLine("Это четное число");
        }
        else
        {
            Console.WriteLine("Это нечетное число");
        }

    }
}

