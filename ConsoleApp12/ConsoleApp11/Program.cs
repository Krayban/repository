﻿using System;

class Program
{
    static void Main(string[] args)
    {
        static void BubbleSort(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
                for (int j = 0; j < array.Length - i - 1; j++)
                {
                    if (array[j] > array[j + 1])
                    {
                        int temp = array[j];
                        array[j] = array[j + 1];
                        array[j + 1] = temp;
                    }
                }
        }

        int[] array = new int[5] { 4, 1, 7, -3, 10 };

        for (int i = 0; i < array.Length; i++)
        {
            BubbleSort(array);
            Console.WriteLine(array[i]);
        }
    }
}
