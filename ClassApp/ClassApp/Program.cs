﻿using System;

namespace ClassApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Person person = new Person();
            person.SetAge(20);
            person.WriteInfo();
        }
    }

    public class Person
    {
        private string firstName = string.Empty;
        private string latestName = string.Empty;
        private string gender = string.Empty;
        private int age = 0;

        public void SetAge(int a)
        {
            if (a > 0)
            {
                age = a;
            }
        }

        public void WriteInfo()
        {
            Console.WriteLine($"Имя: {firstName} Фамилия: {latestName} Пол: {gender} Возраст: {age}");
        }

    }
}