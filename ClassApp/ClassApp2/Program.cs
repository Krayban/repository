﻿using System;

namespace ClassApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Person person = new Person();
            Person person2 = new Person("Anna", "Minagi", "female", 22);
            person.SetFirstName("Inna");
            person.SetLatestName("Fine");
            person.SetGender("female");
            person.SetAge(30);
            person.WriteInfo();
            person2.WriteInfo();

            Person[] pers = new Person[4];
            for(int i = 0; i < pers.Length; i++)
            {
                Console.WriteLine(pers[i]);
                pers[i] = new Person();
                pers[i].SetFirstName(Console.ReadLine());
                pers[i].WriteInfo();
            }
        }
    }

    public class Person
    {
        private string firstName = string.Empty;
        private string latestName = string.Empty;
        private string gender = string.Empty;
        private int age = 0;
        
        public Person()
        {
            firstName = "Tom";
            latestName = "Frans";
            gender = "male";
            age = 21;
        }

        public Person(string firstName, string latestName, string gender, int age)
        {
            this.firstName = firstName;
            this.latestName = latestName;
            this.gender = gender;
            this.age = age;
        }

        public void SetFirstName(string firstName)
        {
            this.firstName = firstName;
        }

        public void SetLatestName(string latestName)
        {
            this.latestName = latestName;
        }

        public void SetGender(string gender)
        {
            this.gender = gender;
        }

        public void SetAge(int a)
        {
            if (a > 0)
            {
                age = a;
            }
        }

        public void WriteInfo()
        {
            Console.WriteLine($"Имя: {firstName} Фамилия: {latestName} Пол: {gender} Возраст: {age}");
        }

    }
}