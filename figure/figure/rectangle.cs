﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace figure
{
    public class Rectangle : Figure
    {
        private float a = 0;
        private float b = 0;
        private float p = 0;

        public Rectangle(float a, float b)
        {
            this.a = a;
            this.b = b;

        }

        public override void WriteName()
        {
            Console.WriteLine("Прямоугольник");
        }

        public override void Perimeter()
        {
            p = a * b;
            Console.WriteLine($"Периметр {p}");
        }


    }

}
