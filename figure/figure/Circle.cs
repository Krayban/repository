﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace figure
{
    public class Circle : Figure
    {
        private float p = 0;
        private float r = 0;
        private const float P = 3.14f;

        public Circle(float r)
        {
            this.r = r;
        }
        public override void WriteName()
        {
            Console.WriteLine("Круг");
        }
        public override void Perimeter()
        {
            p = 2 * P * r;
            Console.WriteLine($"Периметр {p}");
        }
    }
}
