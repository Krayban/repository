﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace figure
{
    public class Figure
    {
        protected string name;

        public Figure()
        {
            name = "фигура";
        }
        public Figure(string name)
        {
            this.name = name;
        }
        public virtual void Area()
        {
            Console.WriteLine("Площадь");
        }
        public virtual void Perimeter()
        {
            Console.WriteLine("Периметр");
        }
        public void SetName(string newName)
        {
            name = newName;
        }
        public virtual void WriteName()
        {
            Console.WriteLine(name);
        }
        
        }
       
    
}
