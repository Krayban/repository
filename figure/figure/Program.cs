﻿using System;

namespace figure
{
    class Program
    {
        static void Main(string[] args)
        {
            Rectangle rectangle = new Rectangle();
            Circle circle = new Circle();

            WriteInfo(rectangle);
            WriteInfo(circle);

            float a = 0;
            float b = 0;
            float p = 3.14f;
            float r = 0;
        }
        static void WriteInfo(Figure figure)
        {
            figure.WriteName();
        }
        
    }
}
