﻿using System;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Введите число");
        string a = Console.ReadLine();
        int i = Convert.ToInt32(a);

        if (i < 10)
        {
            Console.WriteLine("Введённое число < 10. К данному числу будет добавлено 10.");
            Console.WriteLine(i + 10);
        }
        else if (i > 10)
        {
            Console.WriteLine("Введённое число > 10. Из данного числа будет вычтено 10.");
            Console.WriteLine(i - 10);
        }

    }
}

